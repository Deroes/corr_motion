# Author:     Dongwoon Hyun (dongwoon.hyun@stanford.edu)
# Created on: 2020-02-21
import numpy as np
from scipy.special import spence as dilog


## Compute the 2D integral over spatial frequencies u, v.
# This function assumes that the integral is separable over u and v.
# Equation (12) in Hyun and Dahl, JASA 2020.
# Inputs:
#   disp    - 3-tuple of displacements in x, y, z
#   k       - Wavenumber, i.e., 2*pi*f/c
#   z       - Distance between aperture and scatterer planes
#   T1      - Aperture objects for transmit 1
#   R1      - Aperture objects for receive 1
#   T2      - Aperture objects for transmit 2
#   R2      - Aperture objects for receive 2
#   nzu     - Number of zero-crossings of spatial frequencies u to evaluate
#   nzv     - Number of zero-crossings of spatial frequencies v to evaluate
#   dzu     - Spacing of spatial frequencies u (in units of zero-crossings)
#   dzv     - Spacing of spatial frequencies v (in units of zero-crossings)
# Outputs:
#   corr - Correlation of signals from (T1,R1) and (T2,R2) with displacement disp
def corr_motion(disp, k, z, T1, R1, T2, R2, nzu=4, nzv=4, dzu=0.1, dzv=0.1):
    xd, yd, zd = disp
    # Get 2D Fourier-transform functions of each input aperture
    FT1u, FT1v = T1.fourier_transform(k, z)
    FR1u, FR1v = R1.fourier_transform(k, z)
    FT2u, FT2v = T2.fourier_transform(k, z)
    FR2u, FR2v = R2.fourier_transform(k, z)

    # Get spatial frequencies to integrate over based on apertures
    upts, vpts = choose_spat_freqs([T1, R1, T2, R2], k, z, xd, yd, nzu, nzv, dzu, dzv)

    # Define integrand in u (for use with numerical integration packages)
    def integrand_u(u):
        t1 = FT1u(u)
        r1 = FR1u(u)
        t2 = FT2u(u + xd * k / (2 * np.pi * z))
        r2 = FR2u(u + xd * k / (2 * np.pi * z))
        return t1 * r1 * np.conj(t2 * r2) * np.exp(-4j * np.pi * xd * u)

    # Define integrand in v (for use with numerical integration packages)
    def integrand_v(v):
        t1 = FT1v(v)
        r1 = FR1v(v)
        t2 = FT2v(v + yd * k / (2 * np.pi * z))
        r2 = FR2v(v + yd * k / (2 * np.pi * z))
        return t1 * r1 * np.conj(t2 * r2) * np.exp(-4j * np.pi * yd * v)

    # Evaluate integrands at points in u and v
    Iu = integrand_u(upts)
    Iv = integrand_v(vpts)
    # Integrate using trapezoidal rule
    corr = trapz(Iu, upts) * trapz(Iv, vpts)
    # Also apply phase rotation (independent of u, v)
    corr *= np.exp(1j * -k * (2 * zd + (xd * xd + yd * yd) / z))
    # Return complex correlation
    return corr


## Simple trapezoidal rule integration for non-uniformly spaced points.
def trapz(f, x):
    return np.sum((f[:-1] + f[1:]) / 2 * (x[1:] - x[:-1]))


## Choose the spatial frequencies to integrate over
def choose_spat_freqs(apertures, k, z, xd, yd, nzu, nzv, dzu, dzv):
    # Set initial values
    umax = -np.inf
    vmax = -np.inf
    uinc = np.inf
    vinc = np.inf
    # Loop through all apertures
    for a in apertures:
        um, vm, ui, vi = a.spat_freqs(k, z, xd, yd, nzu, nzv, dzu, dzv)
        umax = um if um > umax else umax
        vmax = vm if vm > vmax else vmax
        uinc = ui if ui < uinc else uinc
        vinc = vi if vi < vinc else vinc
    # Get the non-negative integration points and convert to spatial frequencies
    upts = np.arange(0, umax, uinc)
    vpts = np.arange(0, vmax, vinc)
    # Append the negative spatial frequencies as well
    upts = np.concatenate([-np.flip(upts[1:]), upts], axis=0)
    vpts = np.concatenate([-np.flip(vpts[1:]), vpts], axis=0)
    return upts, vpts


## Compute the phase-shift jitter due to decorrelation for speckle signals
def jitter_decorr(mu):
    # Make sure mu is the absolute value of the complex correlation coefficient
    mu = np.abs(mu)
    asinmu = np.arcsin(mu)
    dlogmu = dilog(1 - mu ** 2)  # Spence's function seems to be dilog(1-z)
    theta_var = np.pi ** 2 / 3 - np.pi * asinmu + asinmu ** 2 - dlogmu / 2
    return np.sqrt(theta_var)
