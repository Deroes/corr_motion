"""
This example script computes the coherence as a function of spatial lag. The transmit and
receive apertures are all parabolic-focused rectangular apertures. The transmit aperture
is synthesized retrospectively from four disjoint subapertures.

Scatterer motion is observed to cause decorrelation in the spatial coherence function.

Author:     Dongwoon Hyun (dongwoon.hyun@stanford.edu)
Created on: 2020-03-06
"""

# Code snippet to add python modules to path
import numpy as np
from tqdm import tqdm
from apertures.RectAperture import RectAperture
from corr_motion import corr_motion
import matplotlib.pyplot as plt

# Parameters
c = 1540
z = 50e-3
fc = 7e6
freqs = np.linspace(start=5, stop=9, num=7) * 1e6
bw = 0.6
pitchx = 0.2e-3
pitchy = 7.5e-3

# Number of elements in x, y for transmit and receive apertures
nelxT = 128
nelyT = 1
nelxR = 1
nelyR = 1
nsubap = 4
subapctr = np.arange(nsubap) * nelxT / nsubap * pitchx
subapctr -= np.mean(subapctr)

# Displacements
d_list = np.array([0, 100e-6, 1000e-6, 10000e-6])
ndisps = d_list.size
# Versus azimuthal displacement
yd = 0
zd = 0
for xd in d_list:

    Tx0, Tx1 = np.array([-1, 1]) * nelxT * pitchx / 2 / nsubap
    Ty0, Ty1 = np.array([-1, 1]) * nelyT * pitchy / 2
    Rx0, Rx1 = np.array([-1, 1]) * nelxR * pitchx / 2
    Ry0, Ry1 = np.array([-1, 1]) * nelyR * pitchy / 2
    lags = np.arange(0, nelxT + 1, 4)
    nlags = lags.size

    # Prepare output arrays (complex)
    G12 = np.zeros((nlags, freqs.size)) + 0j

    for ii, lag in enumerate(tqdm(lags)):
        xctrs = np.array([-0.5, 0.5]) * pitchx * lag
        # Loop through frequencies
        for i, f in enumerate(freqs):
            k = 2 * np.pi * f / c
            # Loop through all subaperture cross-products
            for m in range(nsubap):
                for n in range(nsubap):
                    d = np.array([xd, yd, zd]) * (n - m) / nsubap
                    # Transmit and receive apertures
                    T1 = RectAperture(Tx0 + subapctr[m], Tx1 + subapctr[m], Ty0, Ty1)
                    T2 = RectAperture(Tx0 + subapctr[n], Tx1 + subapctr[n], Ty0, Ty1)
                    R1 = RectAperture(Rx0 + xctrs[0], Rx1 + xctrs[0], Ry0, Ry1)
                    R2 = RectAperture(Rx0 + xctrs[1], Rx1 + xctrs[1], Ry0, Ry1)
                    # Compute the integrals
                    G12[ii, i] += corr_motion(d, k, z, T1, R1, T2, R2)

    # Integrate over frequencies (i.e., make simulation broadband)
    F = np.expand_dims(np.exp(-4 * np.pi * ((freqs - fc) / (bw * fc)) ** 2), axis=0)
    G12bb = np.sum(G12 * F, axis=1, keepdims=False)
    g12 = G12bb / np.abs(G12bb[:1])

    # Display plot
    plt.plot(lags, np.abs(g12), label="%d um" % round(xd * 1e6))

plt.title("Spatial Coherence of a Synthetic Aperture with Scatterer Motion")
plt.xlabel("Spatial lag [# pitch]")
plt.ylabel("Correlation coefficient")
plt.legend()
plt.ylim([-0.05, 1.05])
plt.xlim([min(lags), max(lags)])
plt.grid()
plt.savefig("examples/images/motion_coherence_spatial.png")

