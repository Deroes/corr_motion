# Correlation of Pulse-Echo Ultrasound Signals from Diffuse Scatterers in Motion

**corr_motion** is a python-based numerical integration tool for predicting the complex correlation between two arbitrary pulse-echo ultrasound signals obtained from diffuse scatterers undergoing bulk uniform motion.

## Prerequisites

Requirements

- `numpy` for computations
- `scipy` for the dilogarithm function (to compute jitter)

Optional (for example scripts)

- `matplotlib` for plotting
- `tqdm` for progress bar visualization

### Installation for Anaconda

```bash
conda create -n corr_motion python=3 numpy scipy matplotlib tqdm
conda activate corr_motion
```

### Installation for virtualenv (Windows)

```bash
python -m venv .venv
.venv\Scripts\activate
pip install -r requirements.txt
```
The installation procedure is similar for Mac and Linux.

## Motivation

The **complex correlation** between two pulse-echo signals is a key aspect of many ultrasound imaging techniques, such as Doppler imaging, elastography, phase aberration correction, and spatial coherence imaging. For instance, the complex angle of the correlation is used to estimate scatterer motion between two pulse-echo acquisitions, whereas the magnitude of the **correlation coefficient** affects the estimator jitter.

A monochromatic pulse-echo signal can be approximated by a 6D integral over a 2D transmit aperture, a 2D scattering plane, and a 2D receive aperture via the Fresnel approximation:

```math
P(T,S,R) \approx \frac{e^{j2kz}}{(2\pi z)^2}\iiint T(\mathbf{x}_t)S(\mathbf{x}_s)R(\mathbf{x}_r)\exp\left[ \frac{jk}{z} (\mathbf{x}_s^T(\mathbf{x}_s-\mathbf{x}_t-\mathbf{x}_r)) \right] d\mathbf{x}_r d\mathbf{x}_s d\mathbf{x}_t,
```

where $`k=\frac{2\pi f}{c}`$ is the wavenumber, $`z`$ is the distance from the $`T`$ and $`R`$ planes to the $`S`$ plane, and $`\mathbf{x}_t`$, $`\mathbf{x}_s`$, and $`\mathbf{x}_r`$ are the 2D transverse coordinates in each respective plane. The correlation of two pulse-echo signals is the expectation of their product:

```math
\Gamma_{12} = \mathbf{E} \left[ P(T_1,S_1,R_1) P^*(T_2,S_2,R_2) \right].
```

When the scattering medium is diffuse and undergoes a rigid translation between the two pulse-echo signals, i.e. transverse motion $`\mathbf{x}_d`$ and axial motion $`z_d`$, the 12D integral is simplified to a 2D integral (see [paper](https://asa.scitation.org/doi/10.1121/10.0000809) for details):

```math
\Gamma_{12}(k) \approx \frac{|S_0|^2}{(2\pi z)^4}e^{-jk(2z_d + \mathbf{x}_d^T\mathbf{x}_d/z)}\int e^{j4\pi \mathbf{x}_d^T\mathbf{u}}\;\widetilde{T_1}(\mathbf{u})\widetilde{R_1}(\mathbf{u})\left[\widetilde{T_2}\left(\mathbf{u}+\frac{2\pi k}{z}\mathbf{x}_d\right)\widetilde{R_2}\left(\mathbf{u}+\frac{2\pi k}{z}\mathbf{x}_d\right) \right]^* d\mathbf{u},
```

where $`\widetilde{A}(\mathbf{u})`$ is the 2D spatial Fourier transform of aperture $`A`$ at spatial frequency vector $`\mathbf{u}`$.
The monochromatic expression can be extended to broadband by integrating over the frequency responses of the four apertures. If all four apertures have response $`F(k)`$, the broadband correlation is
```math
\Gamma_{12} = \int |F(k)|^4 \Gamma_{12}(k)\,dk.
```

## Code Description

The **corr_motion** package consists of two primary components.

**Aperture definition**
The [apertures](apertures) module provides implementations of common apertures (rectangular, plane wave) and their 2D spatial Fourier transforms. Further detail is provided in [apertures/README.md](apertures/README.md).

**Evaluation of correlation integral**
The [corr_motion.py](corr_motion.py) file contains functions to evaluate the complex correlation integral (Eqs. 12 and 13 in our [paper](https://asa.scitation.org/doi/10.1121/10.0000809)).

The main function is `corr_motion(disp, k, z, T1, R1, T2, R2)`. The user passes in $`\mathbf{x}_d`$, $`z_d`$, $`k`$, $`z`$, and the four aperture objects corresponding to the two pulse-echo signals. The function integration domain is automatically determined according to the four apertures, and can also be set manually. The complex correlation is obtained by numerically integrating the equation above via the trapezoidal rule.

## Example Usage

Examples are provided in the [examples](examples) folder. Detailed descriptions can be found in [examples/README.md](examples/README.md).

## Citing this work

This code is free to use, and is covered by the Apache v2 license. Please cite the following reference if this code is used to publish results:

- [D. Hyun and J. J. Dahl](https://asa.scitation.org/doi/10.1121/10.0000809), "Effects of motion on correlations of pulse-echo ultrasound signals: applications in delay estimation and aperture coherence," The Journal of the Acoustical Society of America **147** (3), pp. 1323-1332 (2020).

```
@article{hyun2020effects,
  title={Effects of motion on correlations of pulse-echo ultrasound signals: Applications in delay estimation and aperture coherence},
  author={Hyun, Dongwoon and Dahl, Jeremy J},
  journal={The Journal of the Acoustical Society of America},
  volume={147},
  number={3},
  pages={1323--1332},
  year={2020},
  publisher={Acoustical Society of America}
}
```
